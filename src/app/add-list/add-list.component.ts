import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { GroceryService } from '../service/grocery.service';

@Component({
  selector: 'app-add-list',
  templateUrl: './add-list.component.html',
  styleUrls: ['./add-list.component.css']
})
export class AddListComponent implements OnInit {

  alert : boolean = false;

  addGroceryListModal = new FormGroup({
    groceryName: new FormControl(''),
    price: new FormControl(''),
    itemId: new FormControl('')
  })

  constructor( private grocery : GroceryService) { }

  ngOnInit(): void {
  }

  collectGrocery(){
    console.log(this.addGroceryListModal.value);
    this.grocery.saveGroceryList(this.addGroceryListModal.value).subscribe((result)=>{
      console.log("result is updated", result)
    })
    this.alert= true;
    this.addGroceryListModal.reset({});
    setTimeout(() => {
      this.alert=false;
    }, 2000);
  }

}
