import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GroceryService {

  url = "http://localhost:9021/api/grocery";
  constructor(public http : HttpClient) { }

  getList(){
    return this.http.get(this.url+"/list");
    
  }

  saveGroceryList(data:any){
    console.log("services " + data);
    return this.http.post(this.url+"/list", data)
  }

  saveGroceryItem(data:any){
    console.log("services " + data);
    return this.http.post(this.url+"/item", data)
  }

  deleteListById(id:number){
    return this.http.delete(`${this.url}/${id}`)
  }

  deleteByItemId(id:number){
    return this.http.delete(`${this.url}/${id}`)
  }

  getListByItemName(id:number){
    return this.http.get(`${this.url}/${id}`)
  }

  updateList(id:any, data:any){
    return this.http.put(`${this.url}/${id}`, data)
  }

  getListByItemId(itemId: string) {
    return this.http.get<any>(`${this.url}/view?itemId=${itemId}`)
  }
}
