import { Component, OnInit } from '@angular/core';
import { GroceryService } from '../service/grocery.service';

@Component({
  selector: 'app-grocery-list',
  templateUrl: './grocery-list.component.html',
  styleUrls: ['./grocery-list.component.css']
})
export class GroceryListComponent implements OnInit {

  constructor(private grocery : GroceryService) { }

  collection:any=[];
  ngOnInit(): void {
    this.grocery.getList().subscribe((result) => {
      console.log(result);
      this.collection=result;
    })
  }
  deleteList(item:number){
    this.collection.splice(item-1, 1)
    this.grocery.deleteListById(item).subscribe((result) => {
      console.log("resulte" + result)
    })
  }

}
