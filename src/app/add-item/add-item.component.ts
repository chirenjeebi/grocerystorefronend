import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { GroceryService } from '../service/grocery.service';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {

  alert : boolean = false;

  addItemModel = new FormGroup({
    itemName: new FormControl(''),
  })

  constructor( private grocery : GroceryService) { }

  ngOnInit(): void {
  }

  collectGroceryItem(){
    console.log(this.addItemModel.value);
    this.grocery.saveGroceryItem(this.addItemModel.value).subscribe((result)=>{
      console.log("Grocery Item added", result)
    })
    this.alert= true;
    this.addItemModel.reset({});
    setTimeout(() => {
      this.alert=false;
    }, 5000);
  }

}
