import { Component, OnInit } from '@angular/core';
import { GroceryService } from '../service/grocery.service';

@Component({
  selector: 'app-grocery-item',
  templateUrl: './grocery-item.component.html',
  styleUrls: ['./grocery-item.component.css']
})
export class GroceryItemComponent implements OnInit {
  itemId: string = '';
  itemList:GroceryItemComponent[] = [];

  constructor(private grocery : GroceryService) { }
  collection:any=[];
  ngOnInit(): void {
    this.grocery.getList().subscribe((result) => {
      console.log(result);
      this.collection=result;
    })
  }
  deleteItem(item:number){
    this.collection.splice(item-1, 1)
    this.grocery.deleteByItemId(item).subscribe((result) => {
      console.log("resulte" + result)
    })
  }

  submitRequest(){
    let html = <HTMLInputElement>document.getElementById('');
    this.itemId = html.value;
    // console.log(this.accountId)
    this.grocery.getListByItemId(this.itemId)
      .subscribe(res => {
        // console.log(res);
        this.itemList = [...res];
      });
  }

}
